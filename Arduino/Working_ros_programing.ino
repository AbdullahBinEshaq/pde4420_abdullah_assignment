#include <Servo.h> // Include Servo.h library
#include <SPI.h> // Include SPI.h library
#include <Wire.h> // Include Wire.h library
#include <Adafruit_GFX.h> // Include Adafruit_GFX.h library
#include <Adafruit_SSD1306.h> // Include Adafruit_SSD1306.h library
#include <ros.h> // Include ROS library
#include <std_msgs/UInt8.h> // Include UInt8.h Source File

#define OLED_RESET 4
Adafruit_SSD1306 display(OLED_RESET);

ros::NodeHandle  nh; // Public node handle allow the program to create publishers and subscribers and take care of serial port communications. 
Servo myservo; // Create Servo object to control a servo. 
int val; // The value assigned to that variable.
int sensorState = 0; // The variable for reading the Seneor status.
int fsrPin = A0; // The FSR and 120 Ohms pulldown are connected to A0.
int fsrReading; // The analog reading from the FSR resistor divider.
const int buzzerPin =12; // Buzzer in linked to Pin #12.
int force_msg; // The FSR Message

void messageCb( const std_msgs::UInt8& cmd_msg){ 
  if(force_msg < 50){
  if(cmd_msg.data == 1){
      digitalWrite(13, HIGH); // LED swtiched On.
      myservo.write(90); // Set servo motor at the angle of the shaft to Open (in 90 degrees).
      digitalWrite(buzzerPin, HIGH); // Buzzer swtiched On.
      delay(50); // Pauses the Buzzer for 50 microseconds to improve simulation performance.
      digitalWrite(buzzerPin, LOW); // Buzzer swtiched Off.
      display.clearDisplay(); // All pixels are Off.
      display.setCursor(5,10);  // Set the coordinates to start writing text in Oled display
      display.setTextSize(2); // Set the font size 2:1 pixel scale
      display.setTextColor(WHITE); // Set the color white text
      display.println("Door Open"); // Print the characters in screen 
      display.display(); // This method for the changes to make effect 
      display.clearDisplay(); // All pixels are Off.
      delay(10000); // Pauses the Servo Motor for 10 seconds to improve simulation performance.
      myservo.write(0); // set servo motor at the angle of the shaft to Colse (in 0 degrees).
      digitalWrite(13, LOW); //LED swtiched Off.
      display.setCursor(1,2);  // Set the coordinates to start writing text in Oled display
      display.setTextSize(2); // Set the font size 2:1 pixel scale
      display.setTextColor(WHITE); // Set the color white text
      display.println("Door Close"); // Print the characters in screen 
      display.display(); // This method for the changes to make effect
      display.clearDisplay(); // All pixels are Off.
  }
  }
}

// The publisher and subscriber which be using. Here instantiate a Publisher with a topic name of "pir_value" and instantiate a Subscriber with a topic name of "door_state".

std_msgs::UInt8 pir_msg;

ros::Publisher pir("pir_value", &pir_msg);

ros::Subscriber<std_msgs::UInt8> door("door_state", &messageCb );



void setup()
{
  // Initialize the ROS node handle, advertise the topic (pir) being published, and subscribe to the topic(door) being susbscribed. 
  myservo.attach(11); // Defined the attached Servo as Output in Pin 2 in the arduino.
  pinMode(2, INPUT); // Defined PIR Senosr as Input in Pin 2 to the arduino.
  pinMode(buzzerPin, OUTPUT); // Defined Buzzer as Output in Pin #12 in the circuit.
  pinMode(13, OUTPUT); // Defined LED as Output in Pin #13 in the circuit
  Serial.begin(9600); // Opens serial port, sets data rate to 9600 bps(bits per second).
  digitalWrite(13, LOW); //LED will Start as 0.
  display.begin(SSD1306_SWITCHCAPVCC, 0x3c); // display 1 op address 0x3C
  display.display(); // This method for the changes to make effect

  nh.initNode();
  nh.advertise(pir);
  nh.subscribe(door);
 
}

void loop()
{
  // In the loop function, the node publishe and call ros::spinOnce() where all of the ROS communication callbacks are handled. 
  myservo.write(0); // Set servo motor at the angle of the shaft (in 0 degrees).
  force_msg = analogRead(fsrPin); // Read force sensor value.
  pir_msg.data = digitalRead(2);
  pir.publish(&pir_msg);
  nh.spinOnce();
  delay(10); // Delay a little bit to improve simulation performance
}
