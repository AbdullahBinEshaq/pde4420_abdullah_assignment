#!/usr/bin/env python
import rospy
from std_msgs.msg import UInt8


rospy.init_node('Smart_door', anonymous=True)
pub = rospy.Publisher('door_state', UInt8, queue_size=10)

def pir_callback(msg):
    pir = msg.data
    if(pir ==1):
        talker(1)
    else:
        talker(0)

def talker(val):
    rospy.loginfo("Motor output %i", val)
    pub.publish(val)
       

def pir_listener():
    rospy.Subscriber("pir_value", UInt8, pir_callback)
    rospy.spin()

	

if __name__ == '__main__':
    pir_listener()
    
